from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os  # provides ways to access the Operating System and allows us to read the environment variables

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(user, password), database="neo4j")


def get_employees(tx):
    query = "MATCH (m:Employee) RETURN m"
    results = tx.run(query).data()
    employees = [{'name': result['m']['name'],
                  'position': result['m']['position']} for result in results]
    return employees


@app.route('/employees', methods=['GET'])
def get_employees_route():
    with driver.session() as session:
        employees = session.read_transaction(get_employees)

    response = {'employees': employees}
    return jsonify(response)


def add_employee(tx, name, position):
    query = "CREATE (m:Employee {name: $name, position: $position})"
    tx.run(query, name=name, position=position)


@app.route('/employees', methods=['POST'])
def add_employee_route():
    name = request.json['name']
    position = request.json['position']

    with driver.session() as session:
        session.write_transaction(add_employee, name, position)

    response = {'status': 'success'}
    return jsonify(response)


def update_employee(tx, id, new_name, new_position):
    query = "MATCH (m:Employee) WHERE ID(m)=$id RETURN m"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (m:Employee) WHERE ID(m)=$id SET m.name=$new_name, m.position=$new_position"
        tx.run(query, id=id, new_name=new_name, new_position=new_position)
        return {'name': new_name, 'position': new_position}


@app.route('/employees/<string:id>', methods=['PUT'])
def update_employee_route(id):
    new_name = request.json['name']
    new_position = request.json['position']

    with driver.session() as session:
        employee = session.write_transaction(
            update_employee, id, new_name, new_position)

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def delete_employee(tx, id):
    query = "MATCH (m:Employee) WHERE ID(m)=$id RETURN m"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (m:Employee) WHERE ID(m)=$id DETACH DELETE m"
        tx.run(query, id=id)
        return {'id': id}


@app.route('/employees/<string:id>', methods=['DELETE'])
def delete_employee_route(id):
    with driver.session() as session:
        employee = session.write_transaction(delete_employee, id)

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def get_subordinates(tx, id):
    query = "MATCH (m:Employee) WHERE ID(m)=$id RETURN m"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (m:Employee)-[r:MANAGES]-(n:Employee) WHERE ID(m)=$id RETURN n"
        results = tx.run(query, id=id)
        employees = [{'name': result['m']['name'],
                      'position': result['m']['position']} for result in results]
        return employees


@app.route('/employees/<string:id>/subordinates', methods=['GET'])
def get_subordinates_route(id):
    with driver.session() as session:
        subordinates = session.write_transaction(get_subordinates, id)

    if not subordinates:
        response = {'message': 'subordinates not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)


def get_departments(tx):
    query = "MATCH (m:Department) RETURN m"
    results = tx.run(query).data()
    departments = [{'name': result['m']['name'],
                    'employee_num': result['m']['employee_num']} for result in results]
    return departments


@app.route('/departments', methods=['GET'])
def get_departments_route():
    with driver.session() as session:
        departments = session.read_transaction(get_employees)

    response = {'departments': departments}
    return jsonify(response)


def get_department_by_employee(tx, id):
    query = "MATCH (m:Employee) WHERE ID(m)=$id RETURN m"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (m:Employee)-[r:WORKS_IN]-(n:Department) WHERE ID(n)=$id RETURN n"
        results = tx.run(query, id=id).data()
        department = [{'name': result['m']['name'],
                       'employee_num': result['m']['employee_num']} for result in results]
        return department


@app.route('/departments/<string:id>/employees', methods=['GET'])
def get_department_by_employee_route(id):
    with driver.session() as session:
        department = session.write_transaction(get_department_employees, id)

    if not department:
        response = {'message': 'department not found'}
        return jsonify(response), 404
    else:
        response = {'department': department}
        return jsonify(response)


def get_department_employees(tx, id):
    query = "MATCH (m:Department) WHERE ID(m)=$id RETURN m"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (m:Employee)-[r:WORKS_IN]-(n:Department) WHERE ID(n)=$id RETURN m"
        results = tx.run(query, id=id)
        employees = [{'name': result['m']['name'],
                      'position': result['m']['position']} for result in results]
        return employees


@app.route('/departments/<string:id>/employees', methods=['GET'])
def get_department_employees_route(id):
    with driver.session() as session:
        employees = session.write_transaction(get_department_employees, id)

    if not employees:
        response = {'message': 'employees not found'}
        return jsonify(response), 404
    else:
        response = {'employees': employees}
        return jsonify(response)


if __name__ == '__main__':
    app.run()
